namespace Astral
open System
open System.Reflection
open System.Linq
open System.Collections.Generic
module Payloads =
    module DataContracts =
        type TypeToContract = Type -> Option<string>
        type ComplexTypeToContracts = TypeToContract -> TypeToContract
        let lift (tt : TypeToContract) : ComplexTypeToContracts = fun (ttd : TypeToContract) -> fun t -> tt t
        let fallback (ttc1 : ComplexTypeToContracts) (ttc2 : ComplexTypeToContracts) : ComplexTypeToContracts =
            fun (tc : TypeToContract) -> 
                fun (t: Type) ->    
                    match ttc1  tc t with
                    | Some a -> Some a
                    | _ -> ttc2 tc t
        let loopback (ttc : ComplexTypeToContracts) : TypeToContract =
            let rec Make (typ : Type) =
                let Checked tt = 
                    if typ = tt then 
                        raise (Exception "123") |> ignore
                    Make(tt)
                ttc Checked typ
            Make 

        let tryGetElementType (alt : Type) =
            let isEnumerableInterface (p : Type) =
                p.IsGenericType && p.GetGenericTypeDefinition() = typedefof<IEnumerable<_>>
            let enumerateType (typ : Type) =
                typ.GetInterfaces() |> Array.tryFind isEnumerableInterface
            if alt.IsArray then
                alt.GetElementType() |> Some
            else
                match enumerateType alt with
                | Some ei -> ei.GetGenericArguments().[0] |> Some
                | _ -> None
        let ArrayLikeTypeMapper (etm : TypeToContract) =
            let typeMapper  typ =
                match tryGetElementType typ with
                | Some et -> etm et |> Option.map (fun s -> s + "[]")
                | None -> None
            typeMapper 

        
